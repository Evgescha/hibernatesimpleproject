package com.hibernatTest.TestHiber;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtil {

    private static SessionFactory factory = null;

    static SessionFactory getSessionFactory() {
        if (factory == null)
            factory = new Configuration().configure().buildSessionFactory();
        return factory;
    }
}
