package com.hibernatTest.TestHiber.entity;

import java.util.Set;

import lombok.Data;

@Data
public class User extends AbstractEntity{
    private String name;
    private String surname;
    private int age;
    private Set<Role> roles;
    private Adres adres;
}
