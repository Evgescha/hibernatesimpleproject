package com.hibernatTest.TestHiber.entity;

import lombok.Data;

@Data
public class Adres extends AbstractEntity {
    private String country;
    private String city;
    private String streed;
    private int home;
    private User user;
}
