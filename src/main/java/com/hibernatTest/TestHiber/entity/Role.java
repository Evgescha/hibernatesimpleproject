package com.hibernatTest.TestHiber.entity;

import java.util.Set;

import lombok.Data;

@Data
public class Role extends AbstractEntity {
    private String title;
    private Set<User> users;
}
