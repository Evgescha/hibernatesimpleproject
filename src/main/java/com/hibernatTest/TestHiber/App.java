package com.hibernatTest.TestHiber;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Start.");

        HibernateSessionFactoryUtil.getSessionFactory();

        System.out.println("End.");
    }
}
